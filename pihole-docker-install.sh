#!/bin/bash
# Name: pihole-docker-install.sh
# Project: https://gitlab.com/knurpht/pihole-installer
# Description: Script to install, configure and run a pihole server using a docker container
# License: GPL v3.0 , see COPYING
# Copyright: ©2018
# Authors: Gertjan Lettink; Wesley Landaal
# Current distros supported: "openSUSE" "Gecko_Linux" "SUSE" "Arch" "Fedora" "Red_Hat" "CentOS" "Debian" "*buntu" "Linux_Mint" "PopOS" "Raspbian"


# BEGIN DISTROS NB Don't use spaces. Add code to the prepare_system() function for docker install. Use _ instead of spaces like Red_Hat
DISTRO_ARRAY=("openSUSE" "Gecko_Linux" "SUSE" "Arch" "Fedora" "Red_Hat" "CentOS" "Debian" "*buntu" "Linux_Mint" "PopOS" "Raspbian") ;
DISTROS=$( printf '%s\n' ${DISTRO_ARRAY[@]} | sort );
# END DISTROS


# BEGIN VARS here
VERSION=0.99
AUTHORS='Gertjan Lettink; Wesley Landaal'
COPYRIGHT='2018'
# interface
WDT="--width=500"
HGT="--height=250"
LHGT="--height=480"
DOCKER_ARCH=$(uname -i);
SUDO="sudo -Sp ''";
# END VARS


# BEGIN FUNCTIONS
check_for_zenity()
{
	ZENITY_INSTALLED=$(which zenity)
	if [ ! "$ZENITY_INSTALLED" ] ; then
		xmessage "Zenity is not installed. \
			Please install before running the script." ;
		exit 4
	fi
}

# initial info screen
show_info()
{
    START_Y_N=$(zenity --question --text="This script will (re)install, (re)configure, remove or update your own Pihole server\n\nVersion: $VERSION\nAUTHORS: $AUTHORS\n©$COPYRIGHT" $WDT $HGT && echo YES || echo NO) ;
    if  [ "$START_Y_N" == "NO" ] ; then
        exit 1
    fi
}

# ask the user for the distro used
get_os()
{
    OS=$(zenity --list --title="Linux Distro" --text="Pick your linux distro" --column="Distro" $DISTROS $WDT $LHGT)  ;	
    if [ "$OS" == "" ] ; then
        get_os
    fi
    if [ "$OS" == "openSUSE" ] || [ "$OS" == "SUSE" ] || [ "$OS" == "Gecko_Linux" ] ; then
            PKG="docker";
            PKGMGR="zypper";
            PKG_OK="--no-confirm";
            PKG_INSTALL="install";
    fi
    if [ "$OS" == "Fedora" ] || [ "$OS" == "Red_Hat" ] || [ "$OS" == "CentOS" ] ; then
            $PKG="docker";
            $PKGMGR="yum";
            $PKG_OK="-y";
            $PKG_INSTALL="install";
    fi
    if [ "$OS" == "Debian" ] || [ "$OS" == "*buntu" ] || [ "$OS" == "Debian" ] || [ "$OS" == "PopOS" ] || [ "$OS" == "Linux_Mint" ] || [ "$OS" == "Raspbian" ] ; then
            $PKG="docker.io";
            $PKGMGR="apt";
            $PKG_OK="-y";
            $PKG_INSTALL="install";
            $FIX_RESOLVE="yes";
    fi
    if [ "$OS" == "Arch" ] ; then
            PKG="docker";
            PKGMGR="pacman";
            PKG_OK="-y";
            PKG_INSTALL="install";
    fi
    confirm_os
}

# have the user confirm his distro to install on
confirm_os()
{
    OS_CONFIRM=$(zenity --question --text="$OS is the distro you entered\n\nSure this the correct one? " $WDT $HGT && echo YES || echo NO) ;
    if  [ "$OS_CONFIRM" == "NO" ] ; then
            get_os
    fi
}

# ask the user for install / remove / reconfigure
get_mode()
{
    MODE=$(zenity --list --title="Choose script mode?" --column="Script-Mode" "Install" "Reconfigure" "Reinstall" "Remove-Pihole" "Remove-Pihole-Docker" "Exit" $WDT $LHGT );
    if [ "$MODE" == "" ] ; then
        show_info
    fi
    if [ "$MODE" == "Remove-Pihole" ] || [ "$MODE" == "Remove-Pihole-Docker" ]; then
        get_sudo_password
        start_remove | zenity --progress --no-cancel --pulsate --auto-close --title="$MODE" --text="$MODE in progress" --percentage=0 $WDT $HGT ;
        $MSG
        show_mode_done
    fi
    if [ "$MODE" == "Exit" ]; then
        exit 1
    fi
}

# start_remove
start_remove()
{
    $SUDO docker rm -f pihole <<<${PASSWD};
    if [ "$DOCKER_ARCH" == "aarch64" ] ; then
        DOCKER_IMAGE="pihole/pihole:v4.0_armhf";
        $SUDO docker rmi pihole/pihole:v4.0_armhf <<<${PASSWD};
    fi
    if [ "$DOCKER_ARCH" == "x86_64" ] ; then
        DOCKER_IMAGE="pihole/pihole:latest";
        $SUDO docker rmi pihole/pihole <<<${PASSWD};
    fi 
    if [ ! "$MODE" == "Remove-Pihole" ]; then
        PKGS_REMOVE="remove $PKG_OK docker* ";
        $SUDO $PKGMGR $PKGS_REMOVE <<<${PASSWD}
        MSG=$(zenity --info --text="Pihole container and docker packages\nhave been removed from your system" $WDT $HGT) ;
    else
        MSG=$(zenity --info --text="Pihole docker container has been removed from your system" $WDT $HGT) ;
    fi
}

# show mode done
show_mode_done()
{
    zenity --info --text="$MODE completed " $WDT $HGT ;
    get_mode
}


# ask the user for the root password to make the 'sudo' commands workif [ $OS == openSUSE || $OS == SUSE ]; then 
get_sudo_password()
{
    zenity --info --text="You will now be asked to enter your root/sudo password\n\nMake sure it's correct" $WDT $HGT ;
    PASSWD=$(zenity --password --title="Root password" $WDT $HGT) ;
    if [ "$PASSWD" == "" ] ; then
        get_sudo_password
    fi
	if  [[ ${?} != 0 || -z ${PASSWD} ]] ; then
        get_sudo_password
	fi
	if ! sudo -kSp '' [ 1 ] <<<${PASSWD} 2>/dev/null ; then
        get_sudo_password

	fi
}

# announce install of dependencies and system preparation
prepare_system_msg()
{
	zenity --info --text="The script will now setup your system for the pihole server" $WDT $HGT ;
	prepare_system 
}

# perform install of depencies and call acivation of the docker.service.
prepare_system()
{	
    get_sudo_password
    DOCKER_INSTALLED=$(which docker);
    MSG=$(zenity --info --text="Docker packages will be installed now\n\nThis may take a minute" $WDT $HGT) ;
    if [ "$DOCKER_INSTALLED" == "" ] ; then
        $MSG;
        $SUDO $PKGMGR $PKG_INSTALL $PKG_OK $PKG <<<${PASSWD} 2>&1 \
        | zenity --progress --no-cancel --pulsate --auto-close --title="Package Install" --text="Installing docker packages" --percentage=0 $WDT $HGT ;
    fi
    if [ "$FIX_RESOLVE" == "yes" ] ; then
 	    $SUDO service systemd-resolved stop <<<${PASSWD}
	    $SUDO systemctl disable systemd-resolved.service <<<${PASSWD}
    fi
	zenity --info --text="Docker packages installed" $WDT $HGT ;
	start_docker
}

# start the docker service
start_docker()
{
	DOCKER_RUNNING=$(systemctl show -p SubState --value docker)
	DOCKER_ENABLED=$(systemctl is-enabled docker.service)
	if [ "$DOCKER_RUNNING" == "running" ] ; then
		MSG=$(zenity --info --text="Docker service already enabled and running" $WDT $HGT);
	else
        MSG=$(zenity --info --text="Docker service now enabled and running" $WDT $HGT) ;
        if [ ! "$DOCKER_ENABLED" == "enabled" ] ; then
            $SUDO systemctl enable docker <<<${PASSWD}
            $SUDO systemctl start docker <<<${PASSWD}
        else
			zenity --info --text="Starting docker service" $WDT $HGT ;
			$SUDO systemctl start docker <<<${PASSWD}
		fi
	fi
    $MSG
	zenity --info --text="Starting configuration of your pihole server" $WDT $HGT ;
}

# ask for the Pihole server IP address | could be picked from ip addr or something like that
get_ipaddress()
{
    GET_IP=$(hostname -I | cut -d' ' -f1)
    IP=$(zenity --entry --text="Enter the pihole IP Address" --entry-text=$GET_IP $WDT $HGT) ;
    if [ "$GET_IP" == "" ] ; then
        get_ipaddress
    fi
}

# have the user confirm the entered IP address
confirm_ipaddress()
{
    IP_CONFIRM=$(zenity --question --text="$IP is the IP you entered\n\nSure this the correct IP address? " $WDT $HGT && echo YES || echo NO) ;
    if  [ "$IP_CONFIRM" == "NO" ] ; then
        get_ipaddress
	fi
}

# ask the user for the path to the pihole docker configs
get_docker_configs()
{
	DOCKER_CONFIGS=$(zenity --entry --text="Enter the path for you Pihole docker configs\n\nLeave default if you don't know what you're changing" --entry-text="/opt/pihole" $WDT $HGT) ;
    if [ "$DOCKER_CONFIGS" == "" ] ; then
        get_docker_configs
    fi	
}

# have the user confirm the entered path for docker configs
confirm_docker_configs()
{
    DOCKER_CONFIGS_CONFIRM=$(zenity --question --text="$DOCKER_CONFIGS is the docker config path you entered\n\nSure this the docker config path you want to use? " $WDT $HGT && echo YES || echo NO) ;
    if  [ "$DOCKER_CONFIGS_CONFIRM" == "NO" ] ; then
        get_docker_configs
	fi
}

# ask the user which port to access the pihole server for port 80
get_port_80()
{
    PORT_80=$(zenity --entry --text="Enter the port to access the pihole server through http\n\nLeave default if you don't know what you're changing" --entry-text="8081" $WDT $HGT) ;
    if [ "$PORT_80" == "" ] ; then
        get_port_80
    fi
}

# have the user confirm the port to access the pihole server for port 80
confirm_port_80()
{
	PORT_80_CONFIRM=$(zenity --question --text="$PORT_80 is the port serving port 80 you entered\n\nSure this is the port you want to use? " $WDT $HGT && echo YES || echo NO) ;
    if  [ "$PORT_80_CONFIRM" == "NO" ] ; then
        get_port_80
	fi
}

# ask the user which port to access the pihole server for port 443 
get_port_443()
{
    PORT_443=$(zenity --entry --text="Enter the port to access the pihole server through https\n\nLeave default if you don't know what you're changing" --entry-text="4443" $WDT $HGT) ;
    if [ "$PORT_443" == "" ] ; then
        get_port_443
    fi
}

# have the user confirm the port to access the pihole server for port 443
confirm_port_443()
{
	PORT_443_CONFIRM=$(zenity --question --text="$PORT_443 is the port serving port 443 you entered\n\nSure this is the port you want to use? " $WDT $HGT && echo YES || echo NO) ;
    if  [ "$PORT_443_CONFIRM" == "NO" ] ; then
        get_port_443
	fi
}

# have the user confirm all entered data
confirm_all()
{
    ALL_CONFIRM=$(zenity --question --text="Are you sure the following entries are correct?\n\nIP Address: $IP\nDocker configs: $DOCKER_CONFIGS\nPort 80: served on $PORT_80\nPort 443: served on $PORT_443\n\nClick No if you're in doubt." $WDT $HGT $WDT $HGT && echo YES || echo NO)
    if  [ "$ALL_CONFIRM" == "NO" ] ; then
        show_info
    else
        create_pihole_password
    fi
}

# create a password for the docker container admin page
create_pihole_password()
{
    zenity --info --text="You will now be asked to set a password for your Pihole admin page" $WDT $HGT ;
    DOCKER_PW=$(zenity --password --title="Pihole password" --text="The Pihole server needs a password to\naccess the admin webpages\n\nCreate a password and remember it somehow." $WDT $HGT) ;
    if [ "$DOCKER_PW" == "" ] ; then
        create_pihole_password
    fi
    confirm_pihole_dhcp
    open_firewall_ports
    MSG=$(zenity --info --text="Pihole docker container being pulled now\n\nThis may take a couple of minutes" $WDT $HGT) ;
    $MSG
    start_docker_pihole | zenity --progress --no-cancel --pulsate --text="Pulling and running pihole container\n\nWait for the progress bar stop pulsating and be filled 100%\nbefore clicking OK" --percentage=0 $WDT $HGT ;
}

# ask the user whether the pihole server is going to be used as a DHCP server
confirm_pihole_dhcp()
{
    CONFIRM_DHCP=$(zenity --question --text="Do you want to use the Pihole server for DHCP?" $WDT $HGT $WDT $HGT && echo YES || echo NO)
    if  [ "$CONFIRM_DHCP" == "NO" ] ; then
        PIHOLE_DHCP_LINE="" ;
    else
        PIHOLE_DHCP_LINE=" -p 67:67/udp " ;
    fi
}

# start the docker container with provided data, remove old container/images
start_docker_pihole()
{
    $SUDO docker rm -f pihole <<<${PASSWD};
    if [ "$DOCKER_ARCH" == "aarch64" ] ; then
        DOCKER_IMAGE="pihole/pihole:v4.0_armhf";
        $SUDO docker rmi pihole/pihole:v4.0_armhf <<<${PASSWD};sleep 1
    fi
    if [ "$DOCKER_ARCH" == "x86_64" ] ; then
        DOCKER_IMAGE="pihole/pihole:latest";
        $SUDO docker rmi pihole/pihole <<<${PASSWD};sleep 1
    fi
    $SUDO docker pull $DOCKER_IMAGE  <<<${PASSWD}  2>&1 ; 
    $SUDO docker run -d \
    --name pihole \
    -p 53:53/tcp -p 53:53/udp \
    -p $PORT_80:80 \
    -p $PORT_443:443 \
    -v "${DOCKER_CONFIGS}/pihole/:/etc/pihole/" \
    -v "${DOCKER_CONFIGS}/dnsmasq.d/:/etc/dnsmasq.d/" \
    -e ServerIP="$IP" \
    --restart=unless-stopped \
    -e WEBPASSWORD=$DOCKER_PW \
    $PIHOLE_DHCP_LINE $DOCKER_IMAGE <<<${PASSWD} ;
}

# open firewall ports using IP tables.
open_firewall_ports()
{
    zenity --info --text="Opening necessary ports in the firewall " $WDT $HGT ;
    $SUDO iptables -A INPUT -p tcp --dport 53 -j ACCEPT <<<${PASSWD};
    $SUDO iptables -A INPUT -p udp --dport 53 -j ACCEPT <<<${PASSWD};
    $SUDO iptables -A INPUT -p tcp --dport 67 -j ACCEPT <<<${PASSWD};
    $SUDO iptables -A INPUT -p udp --dport 67 -j ACCEPT <<<${PASSWD};
    $SUDO iptables -A INPUT -p tcp --dport $PORT_80 -j ACCEPT <<<${PASSWD};
    $SUDO iptables -A INPUT -p udp --dport $PORT_80 -j ACCEPT <<<${PASSWD};
    $SUDO iptables -A INPUT -p tcp --dport $PORT_443 -j ACCEPT <<<${PASSWD};
    $SUDO iptables -A INPUT -p udp --dport $PORT_443 -j ACCEPT <<<${PASSWD};
}

# show DONE and provide URL and password for admin access
show_done()
{
    zenity --info --text="Your password for http://$IP:$PORT_80/admin/ is '$DOCKER_PW'\nYour password for https://$IP:$PORT_443/admin/ is '$DOCKER_PW'\n\nMake sure you open ports 53, 67 udp, $PORT_80 and $PORT_443 in the firewall\n\nTo start using Pihole:\nChange DNS 1 of your router or computer to $IP\nand reconnect your devices with the network.\n\nIn case of firewall issues, first restart your firewall\nto pick up the new rules" $WDT $HGT;
    show_mode_done
}

# END FUNCTIONS

# BEGIN INSTALLER
check_for_zenity
show_info
get_os
get_mode
prepare_system_msg
get_ipaddress
get_docker_configs
get_port_80
get_port_443
confirm_all
show_done
# END INSTALLER
